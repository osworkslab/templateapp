﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TemplateApp.Models;

namespace TemplateApp.Data
{
    public class TemplateContext : IdentityDbContext<ApplicationUser>
    {
        public TemplateContext(DbContextOptions<TemplateContext> options) : base(options)
        {
        }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ShoppingCartItem> ShoppingCartItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
   //     public DbSet<ShoppingCart> ShoppingCart { get; set; }
    }
}
