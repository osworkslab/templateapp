﻿using System;
using System.Linq;
using TemplateApp.Models;

namespace TemplateApp.Data
{
    public class DbInitializer
    {
        public static void Initialize(TemplateContext context)
        {
            //commented out as migrations is used.
            context.Database.EnsureCreated();

            // Look for any students.
          if (context.Employees.Any())
            {
                return;   // DB has been seeded
            }

            var employees = new Employee[]
            {
            new Employee{Name="Carson",Phone="1234567",Email="a@gmail.com",DateJoined=DateTime.Parse("2005-09-01"),Birthday=DateTime.Parse("2005-09-01")},
            new Employee{Name="abc",Phone="1234567",Email="b@gmail.com",DateJoined=DateTime.Parse("2005-09-02"),Birthday=DateTime.Parse("2005-09-01")},
            };

            foreach (Employee s in employees)
            {
                context.Employees.Add(s);
            }
            context.SaveChanges();

            var addresses = new Address[]
            {
            new Address{Addline1="abc",Addline2="Lauterbach",City="Perth",Postcode="1",State="WA"},
            new Address{Addline1="efg",Addline2= "Nomadic10",City="Perth",Postcode="2",State="WA"},
            new Address{Addline1="hij",Addline2="Everest11",City="Perth",Postcode="3",State="WA"},
            new Address{Addline1="kjl",Addline2="SerialConnector",City="Perth",Postcode="4",State="WA"}
            };
            foreach (Address c in addresses)
            {
                context.Addresses.Add(c);
            }
            context.SaveChanges();

            var products = new Product[]
            {
                new Product{ProductName="Pie",ProductType="Bakery",ProductPrice=10.0M,ProductDescription="1",Quantity=1,ProductImage="https://assets.bonappetit.com/photos/57adece91b3340441497571a/16:9/w_1000,c_limit/pear-pie-with-red-wine-and-rosemary1.jpg"},
                new Product{ProductName="Spinach Ricotta Rolls ",ProductType= "Bakery",ProductPrice=5.2M,ProductDescription="2",Quantity=2,ProductImage="https://www.lastminutechef.com/wp-content/uploads/2016/05/spinricottasausagerolls-e1491361298190-600x600.jpg"},
            };
            foreach (Product c in products)
            {
                context.Products.Add(c);
            }

            context.SaveChanges();
        }
    }
}
