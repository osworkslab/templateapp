using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TemplateApp.Data;
using TemplateApp.Models;

namespace TemplateApp.Pages.Employees
{
    public class CreateModel : PageModel
    {
        private readonly TemplateContext _context;

        public CreateModel(Data.TemplateContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty] public Employee Employees { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var emptyEmployees = new Employee();
            if (await TryUpdateModelAsync<Employee>(
                emptyEmployees,
                "Employees", // Prefix for form value.
                s => s.Name, s => s.Phone, s => s.Email, s=>s.Birthday))
            {
                _context.Employees.Add(Employees);
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }

            return null;
        }
    }
}