using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TemplateApp.Data;
using TemplateApp.Models;

namespace TemplateApp.Pages.Employees
{
    public class IndexModel : PageModel
    {
        private readonly TemplateContext _context;

        public IndexModel(TemplateContext context)
        {
            _context = context;
        }

        public PaginatedList<Employee> Employees { get;set; }
        public string NameSort { get; set; }
        public string EmailSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }

        public async Task OnGetAsync(string sortOrder, string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = String.IsNullOrEmpty(sortOrder) ? "lastname_desc" : "";
            EmailSort = sortOrder == "Email" ? "Email_desc" : "Email";

            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            CurrentFilter = searchString;

            IQueryable<Employee> EmployeesList = from s in _context.Employees
                select s;

            if (!String.IsNullOrEmpty(searchString))
            {
                EmployeesList = EmployeesList.Where(s => s.Name.Contains(searchString)
                                                 || s.Phone.Contains(searchString) || s.Email.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "lastname_desc":
                    EmployeesList = EmployeesList.OrderByDescending(s => s.Name);
                    break;
                case "Email":
                    EmployeesList = EmployeesList.OrderBy(s => s.Email);
                    break;
                case "Email_desc":
                    EmployeesList = EmployeesList.OrderByDescending(s => s.Email);
                    break;
                default:
                    EmployeesList = EmployeesList.OrderBy(s => s.Name);
                    break;
            }

            int pageSize = 3;
            Employees = await PaginatedList<Employee>.CreateAsync(
                EmployeesList.AsNoTracking(), pageIndex ?? 1, pageSize);
        }
    }
}
