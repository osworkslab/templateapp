using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TemplateApp.Data;
using TemplateApp.Models;

namespace TemplateApp.Pages.Employees
{
    public class EditModel : PageModel
    {
        private readonly TemplateApp.Data.TemplateContext _context;

        public EditModel(TemplateApp.Data.TemplateContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Employee Employees { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Employees = await _context.Employees.FindAsync(id);

            if (Employees == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var EmployeesToUpdate = await _context.Employees.FindAsync(id);

            if (await TryUpdateModelAsync<Employee>(
                EmployeesToUpdate,
                "Employees",
                s => s.Name, s => s.Phone, s => s.Email))
            {
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }

            return Page();
        }
    }
}
