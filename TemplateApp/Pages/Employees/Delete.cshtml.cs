using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TemplateApp.Data;
using TemplateApp.Models;

namespace TemplateApp.Pages.Employees
{
    public class DeleteModel : PageModel
    {
        private readonly TemplateApp.Data.TemplateContext _context;

        public DeleteModel(TemplateApp.Data.TemplateContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Employee Employees { get; set; }
        public string ErrorMessage { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            Employees = await _context.Employees
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.EmployeeId == id);

            if (Employees == null)
            {
                return NotFound();
            }

            if (saveChangesError.GetValueOrDefault())
            {
                ErrorMessage = "Delete failed. Try again";
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Employees
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.EmployeeId == id);

            if (student == null)
            {
                return NotFound();
            }

            try
            {
                _context.Employees.Remove(student);
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }
            catch (DbUpdateException /* ex */)
            {
                //Log the error (uncomment ex variable name and write a log.)
                return RedirectToAction("./Delete",
                    new { id = id, saveChangesError = true });
            }
        }
    }
}
