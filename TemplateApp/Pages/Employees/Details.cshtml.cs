using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TemplateApp.Data;
using TemplateApp.Models;

namespace TemplateApp.Pages.Employees
{
    public class DetailsModel : PageModel
    {
        private readonly 
            TemplateContext _context;

        public DetailsModel(
            TemplateContext context)
        {
            _context = context;
        }

        public Employee Employees { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Employees = await _context.Employees
             //   .Include(s=>s.AddressId)
               // .ThenInclude(e=>e.Description)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.EmployeeId == id);

            if (Employees == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
