﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using TemplateApp.Data;
using TemplateApp.Models;

namespace TemplateApp.Pages.Paypal
{
    public class PaypalModel : PageModel
    {
        private readonly TemplateApp.Data.TemplateContext _context;

        public PaypalModel(TemplateApp.Data.TemplateContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public ShoppingCart ShoppingCart { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
        /*    if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.ShoppingCart.Add(ShoppingCart);
            await _context.SaveChangesAsync();*/

            return RedirectToPage("./Index");
        }
    }
}