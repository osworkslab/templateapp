﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TemplateApp.Data;
using TemplateApp.Models;

namespace TemplateApp.Pages
{
    public class ProductsModel : PageModel
    {
        private readonly TemplateContext _context;


        public ProductsModel(TemplateContext context)
        {
            _context = context;
        }

        [BindProperty]
        public IList<Product> Products { get; set; }

        public async Task<IActionResult> OnGet()
        {
            Products = await _context.Products.ToListAsync();
         
            return Page();
        }

        /*   public async Task<IActionResult> OnPostAsync()
            {
             /*   if (!ModelState.IsValid)
                {
                    return Page();
                }

                _context.Products.Add(Product);
                await _context.SaveChangesAsync();

                return RedirectToPage("./Index");
            }*/
    }
}