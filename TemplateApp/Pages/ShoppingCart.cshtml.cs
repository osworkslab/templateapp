﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Rewrite.Internal.ApacheModRewrite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PayPal.Api;
using SQLitePCL;
using TemplateApp.Data;
using TemplateApp.Models;
using TemplateApp.ViewModels;

namespace TemplateApp.Pages
{
    public class ShoppingCartModel : PageModel
    {
        public string configclientmode { get; }
        public string Message { get; set; }
        public string configclientid { get; }
        public string configclientsecret { get; }
        public ShoppingCartViewModel CurrentShoppingCart { get; set; }
        private readonly TemplateContext _context;
        private readonly ShoppingCart _shoppingCart;
        public ShoppingCartModel(TemplateContext context, ShoppingCart shoppingCart,IConfiguration configuration)
        {
            _context = context;
            _shoppingCart = shoppingCart;
            configclientmode= configuration["Paypal:mode"];
            configclientid = configuration["Paypal:ClientId"];
            configclientsecret = configuration["Paypal:ClientSecret"];
        }

        public void OnGet()
        {
            Message = "Your application description page.";
        }

        public async Task<IActionResult> OnPostAddToShoppingCartAsync(int productId)
        {
            var selectedProduct = _context.Products.FirstOrDefault(p => p.ProductId == productId);

            if (selectedProduct != null)
            {
                _shoppingCart.AddToCart(selectedProduct, 1);
            }

            var items = _shoppingCart.GetShoppingCartItems();
            _shoppingCart.ShoppingCartItems = items;

            this.CurrentShoppingCart = new ShoppingCartViewModel
            {
                ShoppingCart = _shoppingCart,
                ShoppingCartTotal = _shoppingCart.GetShoppingCartTotal()
            };
            return Page();
        }

        
        private APIContext GetApiContext()
        {
            // Authenticate with PayPal
           
           var accessToken = new OAuthTokenCredential(configclientid,configclientsecret).GetAccessToken();
           // var accessToken = new OAuthTokenCredential(config).GetAccessToken();
            var apiContext = new APIContext(accessToken);

            return apiContext;
        }

        public async Task<IActionResult> OnPostCheckoutAsync()
        {
            /*  var shoppingCartItems = _shoppingCart.GetShoppingCartItems();
            if (shoppingCartItems.Any())
             {
                 // Create an Order object to store info about the shopping cart
                 var order =  _shoppingCart.CreateOrder();
                 // Get PayPal API Context using configuration from web.config
             //   var apiContext = GetApiContext();

                 // Create a new payment object
                 var payment = new Payment
                 {
                     intent = "sale",
                     payer = new Payer
                     {
                         payment_method = "paypal"
                     },
                     transactions = new List<Transaction>
                     {
                         new Transaction
                         {
                             description = $"BeerPal Brewery Shopping Cart Purchase",
                             amount = new Amount
                             {
                                 currency = "USD",
                                 total = (order.Total/100M).ToString(), // PayPal expects string amounts, eg. "20.00",
                                 details = new Details()
                                 {
                                     subtotal = (order.Subtotal/100M).ToString(),
                                     shipping = (order.Shipping/100M).ToString(),
                                     tax = (order.Tax/100M).ToString()
                                 }
                             },
                             item_list = new ItemList()
                             {
                                 items =
                                     order.OrderItems.Select(x => new Item()
                                     {
                                         description = x.Product.ProductName,
                                         currency = "USD",
                                         quantity = x.Quantity.ToString(),
                                         price = (x.Product.ProductPrice/100M).ToString(), // PayPal expects string amounts, eg. "20.00"
                                     }).ToList()
                             }
                         }
                     },
                     redirect_urls = new RedirectUrls
                     {
                         return_url = Url.Action("Return", "ShoppingCart"),
                         cancel_url = Url.Action("Cancel", "ShoppingCart")
                     }
                 };


                            // Send the payment to PayPal
                              var createdPayment = payment.Create(apiContext);

                              // Save a reference to the paypal payment
                              order.PayPalReference = createdPayment.id;
                              _context.SaveChanges();

                              // Find the Approval URL to send our user to
                              var approvalUrl =
                                  createdPayment.links.FirstOrDefault(
                                      x => x.rel.Equals("approval_url", StringComparison.OrdinalIgnoreCase));

                    // Send the user to PayPal to approve the payment
                    return Redirect(approvalUrl.href);
                }

             //   return RedirectToAction("Cart");
                return Page();*/
            return RedirectToPage("Thankyou");
        }

        public ActionResult OnPostReturn(string payerId, string paymentId)
        {
            // Fetch the existing order
            var order = _context.Orders.FirstOrDefault(x => x.PayPalReference == paymentId);

            // Get PayPal API Context using configuration from web.config
            var apiContext = GetApiContext();

            // Set the payer for the payment
            var paymentExecution = new PaymentExecution()
            {
                payer_id = payerId
            };

            // Identify the payment to execute
            var payment = new Payment()
            {
                id = paymentId
            };

            // Execute the Payment
            var executedPayment = payment.Execute(apiContext, paymentExecution);

        //    ClearCart();

            return RedirectToPage("Thankyou");
        }

        public ActionResult OnPostCancel()
        {
            // return View();
            return RedirectToPage("Thankyou");
        }
    }
    }
