﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TemplateApp.Data;
using TemplateApp.Models;

namespace TemplateApp.Pages
{
    public class AboutModel : PageModel
    {
        public string Message { get; set; }
        private readonly TemplateApp.Data.TemplateContext _context;

        public AboutModel(TemplateContext context)
        {
            _context = context;
        }
        public void OnGet()
        {
            Message = "Your application description page.";
        }
    }
}
