﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using TemplateApp.Data;
using TemplateApp.Models;

namespace TemplateApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly TemplateApp.Data.TemplateContext _context;

        public IndexModel(TemplateApp.Data.TemplateContext context)
        {
            _context = context;
        }

        public IList<Product> Product { get;set; }

        public async Task OnGetAsync()
        {
            //Product = await _context.Product
          //      .Include(p => p.Employees).ToListAsync();
        }
    }
}
