using System;
using System.Collections.Generic;


namespace TemplateApp.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
    //    public int AddressId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public DateTime DateJoined { get; set; }
        public DateTime Birthday { get; set; }
    //    public Address Address { get; set; }
    }
}

