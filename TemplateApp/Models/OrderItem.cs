﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TemplateApp.Models
{
    public class OrderItem
    {
        public int OrderItemId { get; set; }
        public int OrderId { get; set; }
        public string Username { get; set; }

       // public int ProductId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }

    }
}