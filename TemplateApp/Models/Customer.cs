﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TemplateApp.Models
{
    public class Customer
    {
            public int CustomerId { get; set; }
            public int AddressId { get; set; }
            public string Name { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public string DateJoined { get; set; }
            public string Birthday { get; set; }
            public Address Address { get; set; }
    }
    }

