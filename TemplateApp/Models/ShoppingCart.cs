﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TemplateApp.Data;

namespace TemplateApp.Models
{
    public class ShoppingCart
    {
        private readonly TemplateContext _appDbContext;
        private ShoppingCart(TemplateContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public string ShoppingCartId { get; set; }

        public List<ShoppingCartItem> ShoppingCartItems { get; set; }

        public static ShoppingCart GetCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?
                .HttpContext.Session;

            var context = services.GetService<TemplateContext>();
            string cartId = session.GetString("CartId") ?? Guid.NewGuid().ToString();

            session.SetString("CartId", cartId);

            return new ShoppingCart(context) { ShoppingCartId = cartId }; 
        }

        public void  AddToCart(Product item, int amount)
        {
            var shoppingCartItem =
                    _appDbContext.ShoppingCartItems.SingleOrDefault(
                        s => s.Product.ProductId == item.ProductId && s.ShoppingCartId == ShoppingCartId);

            if (shoppingCartItem == null)
            {
                shoppingCartItem = new ShoppingCartItem
                {
                    ShoppingCartId = ShoppingCartId,
                    Product = item,
                    Quantity = 1
                };
               // _appDbContext.ShoppingCart.Add(ShoppingCartId);
                _appDbContext.ShoppingCartItems.Add(shoppingCartItem);
            }
            else
            {
                shoppingCartItem.Quantity++;
            }
            _appDbContext.SaveChanges();
        }

        public int RemoveFromCart(Product product)
        {
            var shoppingCartItem =
                    _appDbContext.ShoppingCartItems.SingleOrDefault(
                        s => s.Product.ProductId == product.ProductId && s.ShoppingCartId == ShoppingCartId);

            var localAmount = 0;

            if (shoppingCartItem != null)
            {
                if (shoppingCartItem.Quantity > 1)
                {
                    shoppingCartItem.Quantity--;
                    localAmount = shoppingCartItem.Quantity;
                }
                else
                {
                    _appDbContext.ShoppingCartItems.Remove(shoppingCartItem);
                }
            }

            _appDbContext.SaveChanges();

            return localAmount;
        }

        public List<ShoppingCartItem> GetShoppingCartItems()
        {
            return ShoppingCartItems ??
                   (ShoppingCartItems =
                       _appDbContext.ShoppingCartItems.Where(c => c.ShoppingCartId == ShoppingCartId)
                           .Include(s => s.Product)
                           .ToList());
        }

        public void ClearCart()
        {
            var cartItems = _appDbContext
                .ShoppingCartItems
                .Where(cart => cart.ShoppingCartId == ShoppingCartId);

            _appDbContext.ShoppingCartItems.RemoveRange(cartItems);

            _appDbContext.SaveChanges();
        }

        
        public decimal GetShoppingCartTotal()
        {
            var total = _appDbContext.ShoppingCartItems.Where(c => c.ShoppingCartId == ShoppingCartId)
                .Select(c => c.Product.ProductPrice * c.Quantity).Sum();
            return total;
        }

        public Order CreateOrder()
        {
            // Flat rate shipping
            int shipping = 500;

            // Flat rate tax 10%
            var taxRate = 0.1M;

            var subtotal = _appDbContext.ShoppingCartItems.Sum(x => x.Product.ProductPrice * x.Quantity);
            var tax = Convert.ToInt32((subtotal + shipping) * taxRate);
            var total = subtotal + shipping + tax;

            var Order = new Order()
            {
                OrderDate = DateTime.UtcNow,
                Subtotal = subtotal,
                Shipping = shipping,
                Tax = tax,
                Total = total,
                OrderItems = _appDbContext.ShoppingCartItems.Select(x => new OrderItem()
                {
                    Product = x.Product,
                    Quantity = x.Quantity
                }).ToList()
            };

            _appDbContext.Orders.Add(Order);
            _appDbContext.SaveChanges();
            return Order;
        }
    }
}
