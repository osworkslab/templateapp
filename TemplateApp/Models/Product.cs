﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TemplateApp.Models
{
    public class Product
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string ProductType { get; set; }

        public decimal ProductPrice { get; set; }

        public string ProductDescription { get; set; }

        public int Quantity { get; set; }
        public bool IsOrdered { get; set; }
        public string ProductImage { get; set; }
    }
}
