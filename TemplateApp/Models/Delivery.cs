﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TemplateApp.Models
{
    public class Delivery
    {
        public int DeliveryId { get; set; }

        public int CustomerId { get; set; }

        public int EmployeeId { get; set; }
        public int OrderId { get; set; }
        public int DeliveryStatus { get; set; }
        public String DeliverybyEmployee { get; set; }
        public DateTime DeliveryDateTime { get; set; }
        public String DeliveryDetails { get; set; }

        public int Customer { get; set; }
        public int Employee { get; set; }

    }
}
