﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TemplateApp.Data;

namespace TemplateApp.Models
{
    public class Order
    {
        private readonly TemplateContext _appDbContext;
        public int OrderId { get; set; }
        public int OrderDetailId { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal Total { get; set; }
        public int Tax { get; set; }
        public decimal Subtotal { get; set; }
        public int Shipping { get; set; }
        public int Quantity { get; set; }
        public string PayPalReference { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public Order()
        {
       //     _appDbContext = appDbContext;
            OrderItems = new List<OrderItem>();
        }

        }
    }


